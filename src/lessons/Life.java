package lessons;

import java.util.Random;

/**
 * Created by user on 02.07.2017.
 */
public class Life {

    //Declaring constants
    public static final int SIZE = 50;
    public static final Random RANDOM = new Random();

    private int[][] game;

    private void initializeArray() {
        game = new int[SIZE][SIZE];
    }

    private void fillArray() {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                game[i][j] = RANDOM.nextInt(2);
            }
        }
    }

    private void printModifiedArray() {
        char symbol;
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                symbol = 'X';

                if (game[i][j] == 0) {
                    symbol = ' ';
                }

                System.out.print(symbol);
            }
            System.out.println();
        }
    }


    ////////////////////////
    private int[][] gamenext;

    private void initializeArraynext() {
        gamenext = new int[SIZE][SIZE];
    }

    int transform(int i) {
        return (i < 0) ? (SIZE - 1) : (i > (SIZE - 1)) ? 0 : i;
    }

    private void fillArraynext() {

        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                /*if (i == 0 && j == 0) {
                    gamenext[i][j] = game[game.length - 1][game.length - 1] + game[game.length - 1][j] + game[game.length - 1][j + 1] +
                            game[i][j + 1] + game[i + 1][j + 1] + game[i + 1][j] + game[i + 1][game.length - 1] + game[i][game.length - 1];
                }
                if (i == 0 && j == game.length - 1) {
                    gamenext[i][j] = game[game.length - 1][game.length - 2] + game[game.length - 1][j] + game[game.length - 1][0] + game[0][0] +
                            game[i + 1][0] + game[i + 1][j] + game[i + 1][j - 1] + game[i][j - 1];
                }
                if (i == game.length - 1 && j == game.length - 1) {
                    gamenext[i][j] = game[i - 1][j - 1] + game[i - 1][j] + game[game.length - 2][0] + game[i][0] + game[0][0] + game[0][j] +
                            game[0][j - 1] + game[i][j - 1];
                }
                if (i == game.length - 1 && j == 0) {
                    gamenext[i][j] = game[i - 1][game.length - 1] + game[i - 1][0] + game[i - 1][j + 1] + game[i][j + 1] + game[0][j + 1] +
                            game[0][0] + game[0][game.length - 1] + game[game.length - 1][game.length - 1];
                }
                if (i == 0 && j > 0 && j < game.length - 1) {
                    gamenext[i][j] = game[game.length - 1][j - 1] + game[game.length - 1][j] + game[game.length - 1][j + 1] +
                            game[0][j + 1] + game[i + 1][j + 1] + game[i + 1][j] + game[i + 1][j - 1] + game[i][j - 1];
                }
                if (i > 0 && i < game.length - 1 && j == game.length - 1) {
                    gamenext[i][j] = game[i - 1][j - 1] + game[i - 1][j] + game[i - 1][0] + game[i][0] + game[i + 1][0] + game[i + 1][j] + game[i + 1][j - 1] + game[i][j - 1];
                }
                if (i == game.length - 1 && j > 0 && j < game.length - 1) {

                    gamenext[i][j] = game[i - 1][j - 1] + game[i - 1][j] + game[i - 1][j + 1] + game[i][j + 1] + game[0][j + 1] + game[0][j] + game[0][j - 1] + game[i][j - 1];
                }
                if (i > 0 && i < game.length - 1 && j == 0) {
                    gamenext[i][j] = game[i - 1][game.length - 1] + game[i - 1][j] + game[i - 1][j + 1] + game[i][j + 1] + game[i + 1][j + 1] + game[i + 1][j] + game[i + 1][game.length - 1] + game[i][game.length - 1];
                } else {*/
                gamenext[i][j] = game[transform(i - 1)][transform(j - 1)] + game[transform(i - 1)][transform(j)]
                        + game[transform(i - 1)][transform(j + 1)] + game[transform(i)][transform(j + 1)] + game[transform(i + 1)][transform(j + 1)] + game[transform(i + 1)][transform(j)] + game[transform(i + 1)][transform(j - 1)] + game[transform(i)][transform(j - 1)];
                // }
            }
        }
    }

    public void changegame() {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (game[i][j] == 0) {
                    if (gamenext[i][j] == 3) {
                        game[i][j] = 1;
                    } else {
                        game[i][j] = 0;
                    }
                }

                if (game[i][j] == 1) {
                    if (gamenext[i][j] >= 2 || gamenext[i][j] <= 3) {
                        game[i][j] = 1;
                    } else {
                        game[i][j] = 0;
                    }

                }
            }
        }
    }

///////////////

    private void realization() {
        for (int k = 0; k < 100; k++) {
            fillArraynext();
            changegame();
            printArray();
        }

    }


    public void printArray() {
        char symbol = ' ';
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (game[i][j] == 0){
                    symbol = ' ';
                }else
                if (game[i][j] == 1) {

                    switch (gamenext[i][j]) {

                        case 1:
                            symbol = '#';
                            break;
                        case 2:
                            symbol = '@';
                            break;
                        case 3:
                            symbol = '*';
                            break;
                        case 4:
                            symbol = '$';
                            break;
                        case 6:
                            symbol = '^';
                            break;
                        case 7:
                            symbol = '=';
                            break;
                        case 8:
                            symbol = '-';
                            break;
                    }
                    System.out.print(symbol);
                }

            }System.out.println();
        }
    }


    public void goLife() {
        initializeArray();
        fillArray();
        System.out.println("Initial position");
        printModifiedArray();
        System.out.println();
        System.out.println("Subsequent transformation");
        initializeArraynext();
        //   fillArraynext();
        //   changegame();
        //   printArray();
        realization();
    }

    public static void main(String[] args) {
        Life life = new Life();
        life.goLife();
    }

}


