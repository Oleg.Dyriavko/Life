package com.company;

import java.util.Random;

/**
 * Created by user on 01.07.2017.
 */
public class Universe {

    //глобальная переменная с массивом
    private final int[][] FIELD;
    private int SIZE = 50;
    private final char zero = ' ';
    private final char one = 'X';

    //конструктор

    Universe() {
        FIELD = new int[SIZE][SIZE];
        generate();
    }

    //метод генерации массива
    private void generate() {
        Random random = new Random();
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                FIELD[i][j] = random.nextInt(2);
            }
        }
    }

    //метод печати массива
    public void draw() {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (FIELD[i][j] == 0)
                    System.out.print(zero);
                else System.out.print(one);
            }
            System.out.println();
        }
    }


}


